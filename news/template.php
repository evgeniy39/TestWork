<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

?>
<div class="news-list">
<?
foreach($arResult["ITEMS"] as $v)
{
	?>
	<div class="news-item-good" id="">
		<div class="news-date"><?=$v["DISPLAY_ACTIVE_FROM"]?></div>
        <div class="news-title"><?=$v["NAME"]?></div>
        <div class="news-detail"><?=$v['PREVIEW_TEXT']?></div>
	</div>

<?}?>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

<div class="news-list">
<?
foreach($arResult["MORE"] as $v)
{
	?>
	<div class="news-item-good" id="">
		<div class="news-date"><?=$v["DISPLAY_ACTIVE_FROM"]?></div>
        <div class="news-title"><?=$v["NAME"]?></div>
        <div class="news-detail"><?=$v['PREVIEW_TEXT']?></div>
	</div>
<?}?>
</div>